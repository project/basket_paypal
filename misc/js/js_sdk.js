(function ($, Drupal) {

  'use strict';

  function getEmptyAjaxObject(){
    return Drupal.ajax({
      url: "",
      base: false,
      element: false,
      progress: {type: 'fullscreen'},
    });
  }

  Drupal.behaviors.basketPayPalJsSdk = {
    attach: function (context, settings) {
      $(once('paypal-js', '.js-paypal-javascript-buttons', context)).each(function (){
        if(typeof paypal !== 'undefined'){
          paypal.Buttons({
            style: settings.basketPayPalJsSdk.widgetSettings.style,
            createOrder: function (data, actions) {
              return fetch(Drupal.url('paypal-api/create'), {
                method: "post",
                body: new FormData(document.querySelector('.js-paypal-order-form')),
              })
              .then((res) => {
                return res.json();
              })
              .then((result) => {
                if(result.payPalOrderId){
                  return result.payPalOrderId;
                } else {
                  let emptyAjaxObject = getEmptyAjaxObject();
                  return emptyAjaxObject.success(result, "success");
                }
              });
            },
            onApprove: function (data, actions) {
              return fetch(Drupal.url('paypal-api/capture?' + $('.js-paypal-order-form').serialize() ), {
                method: "post",
                body: JSON.stringify(data),
              })
              .then((res) => {
                return res.json();
              })
              .then((result) => {
                let emptyAjaxObject = getEmptyAjaxObject();
                return emptyAjaxObject.success(result, "success");
              });
            },
          }).render('.js-paypal-javascript-buttons');
        }
      });
    }
  }

})(jQuery, Drupal);
