<?php

namespace Drupal\basket_paypal\FormAlters;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class of OrderFormAlter.
 */
class OrderFormAlter {

  /**
   * PayPalJs object.
   *
   * @var \Drupal\basket_paypal\Services\PayPalJs|object|null
   */
  protected $payPalJs;

  /**
   * Constructs a new instance of the class.
   */
  public function __construct() {
    $this->payPalJs = \Drupal::getContainer()->get('PayPalJs');
  }

  /**
   * Builds the order form and integrates PayPal functionality.
   */
  public function orderForm(&$form, FormStateInterface $formState, $formId) {
    $form['#attributes']['class'][] = 'js-paypal-order-form';
    $formDisplay = $formState->get('form_display');
    if (!empty($formDisplay)) {
      if ($paypalJs = $formDisplay->getComponent('paypal_js')) {
        $form['paypal_js'] = [
          '#theme' => 'basket_paypal_buttons',
          '#weight' => $paypalJs['weight'] ?? '',
          '#attached' => [
            'drupalSettings' => [
              'basketPayPalJsSdk' => [
                'widgetSettings' => $this->payPalJs->getWidgetInfo(),
              ],
            ],
          ],
        ];
      }
    }
  }

}
