<?php

namespace Drupal\basket_paypal;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use PaypalServerSdkLib\Authentication\ClientCredentialsAuthCredentialsBuilder;
use PaypalServerSdkLib\Environment;
use PaypalServerSdkLib\PaypalServerSdkClient;
use PaypalServerSdkLib\PaypalServerSdkClientBuilder;

/**
 * Class of PayPal.
 */
class PayPal {

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Constructs a new PayPal object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function t($string, $args = []) {
    return new TranslatableMarkup($string, $args, ['context' => 'basket_paypal']);
  }

  /**
   * Loads a payment record based on the provided parameters.
   *
   * @param array|null $params
   *   An associative array of parameters that can include:
   *   - id: (optional) The ID of the payment record to load.
   *   - nid: (optional) The node ID associated with the payment.
   *   - sid: (optional) The session ID associated with the payment.
   *   - data: (optional) Additional serialized data to attach to the payment.
   *   - create_new: (optional) If set to TRUE, force the creation of a new
   *     payment object, ignoring existing records.
   *   - amount: (optional) The amount of the payment, required when creating
   *     a new payment record.
   *   - currency: (optional) The currency code of the payment. Defaults to the
   *     configured currency if not provided.
   *
   * @return object|false
   *   Returns the loaded payment object if a match is found, or a newly created
   *   payment object if no match is found and sufficient parameters to create
   *   a new payment exist. Returns FALSE if `create_new` is set and no new
   *   payment can be created due to missing parameters.
   */
  public function load(?array $params) {
    $query = $this->database->select('payments_basket_paypal', 'l');
    $query->fields('l');
    if (!empty($params['id'])) {
      $query->condition('l.id', $params['id']);
    }
    if (!empty($params['nid'])) {
      $query->condition('l.nid', $params['nid']);
    }
    if (!empty($params['sid'])) {
      $query->condition('l.sid', $params['sid']);
    }
    if (empty($params['data'])) {
      $params['data'] = [];
    }
    $payment = $query->execute()->fetchObject();

    if (!empty($params['create_new'])) {
      $payment = FALSE;
    }
    if (empty($payment) && !empty($params['amount'])) {
      $payment = (object) [
        'nid' => !empty($params['nid']) ? $params['nid'] : NULL,
        'sid' => !empty($params['sid']) ? $params['sid'] : NULL,
        'uid' => \Drupal::currentUser()->id(),
        'created' => time(),
        'paytime' => NULL,
        'amount' => $params['amount'],
        'currency' => !empty($params['currency']) ? $params['currency'] : \Drupal::config('basket_paypal.settings')->get('config.currency'),
        'status' => 'new',
        'data' => Json::encode($params['data']),
      ];
      $payment->id = $this->database->insert('payments_basket_paypal')
        ->fields((array) $payment)
        ->execute();
    }
    return $payment;
  }

  /**
   * Updates an existing payment record in the database.
   *
   * @param object $payment
   *   An object containing payment data to update. The object must include an
   *   'id' property to identify the record to be updated. Other properties in
   *   the object specify the fields to update.
   */
  public function update(object $payment) {
    if (!empty($payment->id)) {
      $updateFields = (array) $payment;
      unset($updateFields['id']);
      $this->database->update('payments_basket_paypal')
        ->fields($updateFields)
        ->condition('id', $payment->id)
        ->execute();
    }
  }

  /**
   * Retrieves the configuration settings for the PayPal integration.
   *
   * @return array
   *   An associative array containing the PayPal configuration settings,
   *   including client ID and client secret based on the environment
   *   (sandbox or live).
   */
  public function getConfig() {
    $config = \Drupal::config('basket_paypal.settings')->get('config');
    if (!empty($config['sandbox'])) {
      $config['clientId'] = $config['s_clientId'];
      $config['clientSecret'] = $config['s_clientSecret'];
    }
    return $config;
  }

  /**
   * Creates and returns a PayPal HTTP client instance based.
   *
   * @param array $config
   *   An optional configuration array containing the client ID, client secret,
   *   and sandbox settings. If not provided, a default configuration
   *   will be used.
   */
  public function client(array $config = []): PaypalServerSdkClient {
    if (empty($config)) {
      $config = $this->getConfig();
    }
    $environment = !empty($config['sandbox']) ? Environment::SANDBOX : Environment::PRODUCTION;
    $client = PaypalServerSdkClientBuilder::init();
    $client->clientCredentialsAuthCredentials(
      ClientCredentialsAuthCredentialsBuilder::init(
        $config['clientId'],
        $config['clientSecret'],
      )
    );
    $client->environment($environment);

    return $client->build();
  }

}
