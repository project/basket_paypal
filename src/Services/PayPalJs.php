<?php

namespace Drupal\basket_paypal\Services;

use Drupal\basket_paypal\PayPal;
use Drupal\Core\Url;
use PaypalServerSdkLib\Models\Builders\AmountWithBreakdownBuilder;
use PaypalServerSdkLib\Models\Builders\OrderApplicationContextBuilder;
use PaypalServerSdkLib\Models\Builders\OrderRequestBuilder;
use PaypalServerSdkLib\Models\Builders\PurchaseUnitRequestBuilder;
use PaypalServerSdkLib\Models\CheckoutPaymentIntent;

/**
 * Class of PayPalJs.
 */
class PayPalJs {

  const PAY_PAL_JS_LAYOUT = [
    'vertical' => 'vertical',
    'horizontal' => 'horizontal',
  ];

  const PAY_PAL_JS_COLORS = [
    'gold' => 'gold',
    'blue' => 'blue',
    'silver' => 'silver',
    'white' => 'white',
    'black' => 'black',
  ];

  const PAY_PAL_JS_SHAPE = [
    'rect' => 'rect',
    'pill' => 'pill',
    'sharp' => 'sharp',
  ];

  const CURRENCY = [
    'AUD',
    'BRL',
    'CAD',
    'CZK',
    'DKK',
    'EUR',
    'HKD',
    'HUF',
    'ILS',
    'JPY',
    'MYR',
    'MXN',
    'TWD',
    'NZD',
    'NOK',
    'PHP',
    'PLN',
    'GBP',
    'SGD',
    'SEK',
    'CHF',
    'THB',
    'USD',
  ];

  /**
   * ContainerInterface.
   *
   * @var \Drupal\Component\DependencyInjection\ContainerInterface|null
   */
  protected $container;

  /**
   * PayPal object.
   *
   * @var \Drupal\basket_paypal\PayPal
   */
  protected $payPal;

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * Configuration.
   *
   * @var array|null
   */
  protected $config;

  /**
   * Initializes the class with the necessary dependencies and configurations.
   */
  public function __construct(PayPal $pay_pal) {
    $this->container = \Drupal::getContainer();
    $this->payPal = $pay_pal;
    $this->basket = $this->container->get('Basket');
    $this->config = \Drupal::config('basket_paypal_js.settings')->get('config');
  }

  /**
   * Retrieves the default settings for the PayPal widget configurations.
   */
  public function getDefaultSettings() {
    return [
      'style' => [
        'layout' => $this->config['style']['layout'] ?? key(static::PAY_PAL_JS_LAYOUT),
        'color' => $this->config['style']['color'] ?? key(static::PAY_PAL_JS_COLORS),
        'shape' => $this->config['style']['shape'] ?? key(static::PAY_PAL_JS_SHAPE),
      ],
    ];
  }

  /**
   * Retrieves widget information by merging the provided configuration data.
   */
  public function getWidgetInfo($data = []) {
    $info = $data['config'] ?? [];
    return array_replace_recursive($this->getDefaultSettings(), $info);
  }

  /**
   * Creates an order based on provided payment information and parameters.
   */
  public function createOrder($parmentInfo, $params = []) {
    $payment = $this->payPal->load($parmentInfo);
    foreach ($parmentInfo as $paramKey => $param) {
      if (property_exists($payment, $paramKey)) {
        $payment->{$paramKey} = $param;
      }
    }
    $this->payPal->update($payment);

    $_SESSION['basket_paypal_js_last_pay'] = $payment->id;
    $result = [
      'payment' => clone $payment,
    ];

    $payment->result_url = Url::fromRoute('basket_paypal.pages', ['page_type' => 'result'], ['absolute' => TRUE])->toString();
    $payment->cancel_url = Url::fromRoute('basket_paypal.pages', ['page_type' => 'cancel'], ['absolute' => TRUE])->toString();

    $config = $this->payPal->getConfig();

    $defParams = [
      'intent' => CheckoutPaymentIntent::CAPTURE,
      'purchase_units' => [
        PurchaseUnitRequestBuilder::init(
          AmountWithBreakdownBuilder::init(
            $payment->currency ?? 'USD',
            $payment->amount ?? 0,
          )->build(),
        )->referenceId($payment->id)->build(),
      ],
      'application_context' => OrderApplicationContextBuilder::init()
        ->cancelUrl($payment->cancel_url)
        ->returnUrl($payment->result_url)
        ->build(),
    ];
    $params = array_replace_recursive($defParams, $params);

    // Alter:
    \Drupal::moduleHandler()->alter('basket_paypal_order_params', $params, $payment, $config);

    $_SESSION['basket_paypal_last_pay'] = $payment->id;
    $client = $this->payPal->client($config);
    $orderRequestBuilder = OrderRequestBuilder::init($params['intent'], $params['purchase_units']);

    if (!empty($params['application_context'])) {
      $orderRequestBuilder->applicationContext($params['application_context']);
    }

    if (!empty($params['payment_source'])) {
      $orderRequestBuilder->paymentSource($params['payment_source']);
    }

    $collect = [
      'body' => $orderRequestBuilder->build(),
      'prefer' => 'return=representation',
    ];
    try {
      // Call API with your client and get a response for your call.
      $response = $client->getOrdersController()->ordersCreate($collect);
      $result['result'] = $response->getResult();
    }
    catch (\Exception $ex) {
      \Drupal::logger('BasketPayPal')->notice($ex->getMessage());
    }

    return $result;
  }

  /**
   * Retrieves the current currency code associated with the basket.
   */
  public function getCurrency() {
    $currencyCurrent = $this->basket->currency()->getCurrent();
    $iso = $this->basket->currency()->load($currencyCurrent)->iso;
    if (in_array($iso, static::CURRENCY)) {
      return $iso;
    }
    else {
      return 'USD';
    }
  }

}
