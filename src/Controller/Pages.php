<?php

namespace Drupal\basket_paypal\Controller;

use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class of Pages.
 */
class Pages {

  /**
   * PayPal service.
   *
   * @var \Drupal\basket_paypal\PayPal|object|null
   */
  protected $payPal;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->payPal = \Drupal::getContainer()->get('PayPal');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::getContainer()->get('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
  }

  /**
   * Translates and returns the provided title string with arguments.
   */
  public function title(array $_title_arguments = [], $_title = '') {
    return $this->payPal->t($_title);
  }

  /**
   * Handles different page types for PayPal integrations.
   */
  public function pages($page_type) {
    $element = [];

    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->payPal->getConfig();

    switch ($page_type) {
      case'pay':
        $is_404 = TRUE;
        if (!empty($this->query['pay_id'])) {
          $payment = $this->payPal->load([
            'id' => $this->query['pay_id'],
          ]);
          if (!empty($payment->status) && $payment->status == 'new') {
            $is_404 = FALSE;
            $element['form'] = \Drupal::formBuilder()->getForm('\Drupal\basket_paypal\Form\PaymentForm', $payment);
          }
        }
        if ($is_404) {
          throw new NotFoundHttpException();
        }
        break;

      case'webhook':
        if (!$_POST) {
          $_POST = @json_decode(file_get_contents('php://input'), TRUE);
        }
        if (!empty($_POST['event_type'])) {
          switch ($_POST['event_type']) {
            case 'CHECKOUT.ORDER.APPROVED':
              $this->updateOrderInfo($_POST, $_POST['event_type']);

              if (!empty($_POST['resource']['id'])) {
                try {
                  $client = $this->payPal->client($config);
                  $client->getOrdersController()->ordersCapture([
                    'id' => $_POST['resource']['id'],
                    'prefer' => 'return=representation',
                  ]);
                }
                catch (\Exception $ex) {
                  \Drupal::logger('BasketPayPal_CHECKOUT.ORDER.APPROVED')->error($ex->getMessage());
                }
              }
              break;

            case 'PAYMENT.CAPTURE.COMPLETED':
              if (!empty($_POST['resource']['supplementary_data']['related_ids']['order_id'])) {
                $payPalOrderId = $_POST['resource']['supplementary_data']['related_ids']['order_id'];
                $client = $this->payPal->client($config);

                try {
                  $order = $client->getOrdersController()->ordersGet([
                    'id' => $payPalOrderId,
                  ]);
                  $result = $order->getResult();
                  $status = $result->getStatus();

                  if ($status === 'COMPLETED') {
                    $purchaseUnits = $result->getPurchaseUnits();

                    foreach ($purchaseUnits as $purchaseUnit) {
                      $referenceId = $purchaseUnit->getReferenceId();

                      if (!empty($referenceId)) {
                        @[$orderId, $rand] = explode('-', $referenceId);

                        $payment = $this->payPal->load([
                          'id' => $orderId,
                        ]);
                        if (!empty($payment)) {
                          $dataPay = Json::decode($payment->data);
                          $dataPay['pre_pay'] = $dataPay['pre_pay'] ?? $dataPay;
                          $payment->data = Json::encode($dataPay);
                          $payment->status = $status;
                          $payment->paytime = time();

                          $this->payPal->update($payment);

                          if (!empty($payment->nid) && !empty($this->basket) && $payment->status === 'COMPLETED') {
                            if (method_exists($this->basket, 'paymentFinish')) {
                              $this->basket->paymentFinish($payment->nid);
                            }
                          }
                        }
                      }
                    }
                  }
                }
                catch (\Exception $ex) {
                  \Drupal::logger('BasketPayPal_PAYMENT.CAPTURE.COMPLETED')->error($ex);
                }
              }
              break;

            case'CHECKOUT.ORDER.COMPLETED':
              $this->updateOrderInfo($_POST, $_POST['event_type']);
              break;
          }
        }
        break;

      case'result':
        $success = !empty($config['success'][$langcode]) ? $config['success'][$langcode] : $config['success']['en'];
        $element += [
          '#theme' => 'basket_paypal_pages',
          '#info' => [
            'text' => [
              '#type' => 'processed_text',
              '#text' => !empty($success['value']) ? $success['value'] : '',
              '#format' => !empty($success['format']) ? $success['format'] : NULL,
            ],
            'type' => 'success',
          ],
          '#attached' => [
            'library' => ['basket_paypal/css'],
          ],
        ];
        break;

      case'cancel':
        $element += [
          '#theme' => 'basket_paypal_pages',
          '#info' => [
            'text' => [
              '#type' => 'processed_text',
              '#text' => '<p class="text-align-center"><strong>' . $this->payPal->t('No payment data.') . '</strong></p>',
              '#format' => 'full_html',
            ],
            'type' => 'cancel',
          ],
          '#attached' => [
            'library' => ['basket_paypal/css'],
          ],
        ];
        break;
    }

    return $element;
  }

  /**
   * Updates the order information based on the provided data and type.
   */
  private function updateOrderInfo($data, $type) {
    $orderId = NULL;
    $status = $data['resource']['status'] ?? NULL;
    if (!empty($data['resource']['purchase_units'][0]['reference_id'])) {
      @[$orderId, $rand] = explode('-', $data['resource']['purchase_units'][0]['reference_id']);
    }
    if (!empty($orderId)) {
      $payment = $this->payPal->load([
        'id' => $orderId,
      ]);
      if (!empty($payment)) {
        $dataPay = Json::decode($payment->data);
        $dataPay['pre_pay'] = $dataPay['pre_pay'] ?? $dataPay;
        $dataPay[$type] = $data;

        $payment->paytime = time();
        $payment->data = Json::encode($dataPay);
        $payment->status = $status ?? 'new-update';

        $this->payPal->update($payment);

        if (!empty($payment->nid) && !empty($this->basket) && $payment->status == 'COMPLETED') {
          if (method_exists($this->basket, 'paymentFinish')) {
            $this->basket->paymentFinish($payment->nid);
          }
        }
      }
    }
  }

}
