<?php

namespace Drupal\basket_paypal\Controller;

use Drupal\basket\Entity;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\ScrollTopCommand;
use Drupal\Core\Form\FormState;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use PaypalServerSdkLib\Models\Builders\NameBuilder;
use PaypalServerSdkLib\Models\Builders\PaymentSourceBuilder;
use PaypalServerSdkLib\Models\Builders\PaypalWalletBuilder;
use PaypalServerSdkLib\Models\Builders\PhoneNumberBuilder;
use PaypalServerSdkLib\Models\Builders\PhoneWithTypeBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class of ApiPage.
 */
class ApiPage {

  /**
   * PayPal service.
   *
   * @var \Drupal\basket_paypal\PayPal
   */
  protected $payPal;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $payPalJs;

  /**
   * Config data.
   *
   * @var array|mixed|null
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->payPal = \Drupal::getContainer()->get('PayPal');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::getContainer()->get('Basket') : NULL;
    $this->payPalJs = \Drupal::getContainer()->get('PayPalJs');
    $this->config = \Drupal::config('basket_paypal_js.settings')->get('config');
  }

  /**
   * Creates and returns a new entity with default and provided data.
   */
  public function getEntity($data = []) {
    $nodeDefault = [
      'type' => 'basket_order',
      'title' => 'new',
    ];
    if (!empty($data)) {
      $nodeDefault += $data;
    }
    $entity = Node::create($nodeDefault);
    $entity->enforceIsNew();
    return $entity;
  }

  /**
   * Handles the different page.
   */
  public function page($page_type) {
    $element = [];
    switch ($page_type) {

      case'create':
        $errors = [];
        $formData = \Drupal::request()->request->all();
        if ($this->config['validate']) {
          $entity = $this->getEntity();
          $formMode = 'default';
          $formModeSettings = $this->basket->getSettings('order_form', 'config.form_mode');
          if (!empty($formModeSettings) && !empty(\Drupal::getContainer()->get('entity_display.repository')->getFormModes('node')[$formModeSettings])) {
            $formMode = $formModeSettings;
          }
          $formState = new FormState();
          $formState->setValues($formData);
          $formObject = \Drupal::entityTypeManager()->getFormObject($entity->getEntityTypeId(), $formMode);
          $formObject->setEntity($entity);
          $formState->setProgrammed();
          $form = \Drupal::formBuilder()->buildForm($formObject, $formState);
          \Drupal::formBuilder()->validateForm($formData['form_id'], $form, $formState);
          $errors = $formState->getErrors();
        }
        if (!empty($errors)) {
          $response = new AjaxResponse();
          $wrapper = $form['actions']['submit']['#ajax']['wrapper'];
          $selector = '#' . $wrapper;
          $response->addCommand(new ReplaceCommand($selector, $form));
          $response->addCommand(new ScrollTopCommand($selector));
          return $response;
        }
        else {
          $sum = $this->basket->cart()->getTotalSum();

          $currencyCurrent = $this->basket->currency()->getCurrent();
          $currentIso = $this->basket->currency()->load($currencyCurrent)->iso;
          $currencyIso = $this->payPalJs->getCurrency();
          if ($currentIso != $currencyIso) {
            $currencyPay = $this->basket->currency()->loadByIso($currencyIso);
            $this->basket->currency()->priceConvert($sum, $currencyCurrent, FALSE, $currencyPay->id);
          }

          $payment = [
            'amount' => number_format($sum, 2, '.', ''),
            'currency' => $currencyIso,
          ];
          if (!empty($_SESSION['basket_paypal_js_last_pay'])) {
            $payment['id'] = $_SESSION['basket_paypal_js_last_pay'];
          }
          $params = [];
          if ($this->config['validate']) {
            $walletBuilder = PaypalWalletBuilder::init();
            $walletBuilder->emailAddress($formData['basket_order_mail'][0]['value'] ?? '');
            $walletBuilder->name(
              NameBuilder::init()
                ->givenName($formData['basket_order_name'][0]['value'] ?? '')
                ->surname($formData['basket_order_surname'][0]['value'] ?? '')
                ->build()
            );
            $phoneNumber = !empty($formData['basket_order_phone'][0]['value']) ? preg_replace('/[^0-9]/', '', $formData['basket_order_phone'][0]['value']) : '';
            $walletBuilder->phone(
              PhoneWithTypeBuilder::init(PhoneNumberBuilder::init($phoneNumber)->build())
                ->phoneType('MOBILE')
                ->build()
            );
            $wallet = $walletBuilder->build();
            $params['payment_source'] = PaymentSourceBuilder::init()
              ->paypal($wallet)
              ->build();
          }
          $result = $this->payPalJs->createOrder($payment, $params);
          if (!empty($result['result'])) {
            $result['payment']->data = Json::encode($result['result']);
            $result['payment']->status = $result['result']->getStatus();
            $this->payPal->update($result['payment']);
            return new JsonResponse(['payPalOrderId' => $result['result']->getId()]);
          }
        }
        break;

      case 'capture':
        $request = \Drupal::request();
        $contentString = $request->getContent();
        $query = $request->query->all();
        if (!empty($contentString)) {
          $content = json_decode($contentString);
          if (!empty($content->orderID)) {
            $entity = $this->getEntity($query);
            $entity->save();

            $payment = $this->payPal->load(['id' => $_SESSION['basket_paypal_js_last_pay']]);
            unset($_SESSION['basket_paypal_js_last_pay']);
            $payment->nid = $entity->id();

            $client = $this->payPal->client();
            try {
              $response = $client->getOrdersController()->ordersGet([
                'id' => $content->orderID,
              ]);
              $result = $response->getResult();
              $status = $result->getStatus();
              $payment->data = Json::encode($result);
              $payment->status = $status;
              if ($status === 'APPROVED') {
                $payment->paytime = \Drupal::time()->getRequestTime();
              }
            }
            catch (\Exception $e) {
              \Drupal::logger('basket_paypal')->error($e->getMessage());
            }

            $this->payPal->update($payment);

            $formState = new FormState();
            if (!empty($query['delivery'])) {
              $formState->setValue('basket_delivery', $query['delivery']);
            }
            $paySystemId = array_search('basket_paypal_js', \Drupal::getContainer()->get('Basket')->getSettings('payment_services'));
            if ($paySystemId !== FALSE) {
              $formState->setValue('basket_payment', $paySystemId);
            }
            (new Entity)->insertOrder($entity, $formState);

            \Drupal::getContainer()->get('Basket')->paymentFinish($entity->id());

            $response = new AjaxResponse();
            $redirectUrl = Url::fromRoute('basket_paypal.pages', ['page_type' => 'result'], ['absolute' => TRUE]);
            $response->addCommand(new RedirectCommand($redirectUrl->toString()));
            return $response;
          }

        }
        break;
    }
    return $element;
  }

}
