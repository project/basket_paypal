<?php

namespace Drupal\basket_paypal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class of JavascriptSDKSettingsForm.
 */
class JavascriptSDKSettingsForm extends ConfigFormBase {

  const SETTINGS = 'basket_paypal_js.settings';

  /**
   * PayPal service.
   *
   * @var \Drupal\basket_paypal\PayPal
   */
  protected $payPal;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * PayPalJs object.
   *
   * @var \Drupal\basket_paypal\Services\PayPalJs|object|null
   */
  protected $payPalJs;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->payPal = \Drupal::getContainer()->get('PayPal');
    $this->ajax = [
      'wrapper' => 'paypal-js-preview',
      'callback' => [$this, 'ajaxSubmit'],
    ];
    $this->config = \Drupal::config('basket_paypal.settings');
    $this->payPalJs = \Drupal::getContainer()->get('PayPalJs');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'basket_paypal_js_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS)->get('config');
    $form += [
      'status_messages' => [
        '#type' => 'status_messages',
      ],
      '#title' => $this->payPal->t('Basket PayPal settings'),
      '#parents' => ['config'],
      '#tree' => TRUE,
    ];
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value' => $config['enable'] ?? 0,
    ];
    $form['validate'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable form validation'),
      '#default_value' => $config['validate'] ?? 0,
    ];
    $form['style'] = [
      'layout' => [
        '#type' => 'select',
        '#title' => t('Layout'),
        '#options' => $this->payPalJs::PAY_PAL_JS_LAYOUT,
        '#ajax' => $this->ajax,
        '#default_value' => $config['style']['layout'] ?? '',
      ],
      'color' => [
        '#type' => 'select',
        '#title' => t('Color'),
        '#options' => $this->payPalJs::PAY_PAL_JS_COLORS,
        '#ajax' => $this->ajax,
        '#default_value' => $config['style']['color'] ?? '',
      ],
      'shape' => [
        '#type' => 'select',
        '#title' => t('Shape'),
        '#options' => $this->payPalJs::PAY_PAL_JS_SHAPE,
        '#ajax' => $this->ajax,
        '#default_value' => $config['style']['shape'] ?? '',
      ],
    ];
    $form['preview'] = [
      '#theme' => 'basket_paypal_buttons',
      '#weight' => 1000,
      '#prefix' => '<div id="paypal-js-preview">',
      '#suffix' => '</div>',
      '#attached' => [
        'drupalSettings' => [
          'basketPayPalJsSdk' => [
            'widgetSettings' => $this->payPalJs->getWidgetInfo($form_state->getUserInput()),
          ],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $config = $formState->getValue('config');
    if (isset($config['actions'])) {
      unset($config['actions']);
    }
    $this->config(static::SETTINGS)
      ->set('config', $config)
      ->save();
    parent::submitForm($form, $formState);
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    return $form['preview'];
  }

}
