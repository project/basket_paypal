<?php

namespace Drupal\basket_paypal\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;

/**
 * Class of SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * PayPal service.
   *
   * @var \Drupal\basket_paypal\PayPal
   */
  protected $payPal;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->payPal = \Drupal::getContainer()->get('PayPal');
    $this->ajax = [
      'wrapper' => 'fondy_settings_form_ajax_wrap',
      'callback' => '::ajaxSubmit',
    ];
    $this->config = \Drupal::config('basket_paypal.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'basket_paypal_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form += [
      '#prefix' => '<div id="' . $this->ajax['wrapper'] . '">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages',
      ],
      '#title' => $this->payPal->t('Basket PayPal settings'),
    ];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $languages = \Drupal::languageManager()->getLanguages();
    if (count($languages) > 1) {
      $options = [];
      foreach ($languages as $language) {
        $options[$language->getId()] = $language->getName();
      }
      $form['language'] = [
        '#type' => 'select',
        '#title' => t('Language'),
        '#options' => $options,
        '#ajax' => $this->ajax,
        '#default_value' => $langcode,
      ];
    }
    if (!empty($values['language'])) {
      $langcode = $values['language'];
    }
    $isSandbox = !empty($values) ? $values['config']['sandbox'] : $this->config->get('config.sandbox');
    $form['config'] = [
      '#tree' => TRUE,
      'sandbox' => [
        '#type' => 'checkbox',
        '#title' => $this->payPal->t('Sandbox'),
        '#ajax' => $this->ajax,
        '#default_value' => $this->config->get('config.sandbox'),
      ],
      'clientId' => [
        '#type' => empty($isSandbox) ? 'textfield' : 'hidden',
        '#title' => $this->payPal->t('Client ID'),
        '#required' => empty($isSandbox),
        '#default_value' => $this->config->get('config.clientId'),
      ],
      'clientSecret' => [
        '#type' => empty($isSandbox) ? 'textfield' : 'hidden',
        '#title' => $this->payPal->t('Client Secret'),
        '#required' => empty($isSandbox),
        '#default_value' => $this->config->get('config.clientSecret'),
      ],
      's_clientId' => [
        '#type' => !empty($isSandbox) ? 'textfield' : 'hidden',
        '#title' => $this->payPal->t('Client ID') . ' (' . $this->payPal->t('Sandbox') . ')',
        '#required' => !empty($isSandbox),
        '#default_value' => $this->config->get('config.s_clientId'),
      ],
      's_clientSecret' => [
        '#type' => !empty($isSandbox) ? 'textfield' : 'hidden',
        '#title' => $this->payPal->t('Client Secret') . ' (' . $this->payPal->t('Sandbox') . ')',
        '#required' => !empty($isSandbox),
        '#default_value' => $this->config->get('config.s_clientSecret'),
      ],
      [
        '#theme' => 'table',
        '#rows' => [
          [
            'Webhook URL',
            Url::fromRoute('basket_paypal.pages', ['page_type' => 'webhook'], ['absolute' => TRUE])->toString(),
          ],
        ],
      ],
      'success' => [
        '#type' => 'text_format',
        '#title' => $this->payPal->t('Successful payment') . ' (' . $langcode . ')',
        '#default_value' => $this->config->get('config.success.' . $langcode . '.value'),
        '#format' => $this->config->get('config.success.' . $langcode . '.format'),
        '#parents' => ['config', 'success', $langcode],
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#name' => 'save',
        '#value' => t('Save configuration'),
        '#attributes' => [
          'class' => ['button--primary'],
        ],
        '#ajax' => $this->ajax,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('basket_paypal.settings');
      $configSave = $form_state->getValue('config');
      if (!empty($preConfig = \Drupal::config('basket_paypal.settings')->get('config'))) {
        if (!empty($preConfig['success'])) {
          $configSave['success'] += $preConfig['success'];
        }
      }
      $config->set('config', $configSave);
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
