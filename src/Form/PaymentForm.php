<?php

namespace Drupal\basket_paypal\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use PaypalServerSdkLib\Models\Builders\AmountWithBreakdownBuilder;
use PaypalServerSdkLib\Models\Builders\OrderApplicationContextBuilder;
use PaypalServerSdkLib\Models\Builders\OrderRequestBuilder;
use PaypalServerSdkLib\Models\Builders\PurchaseUnitRequestBuilder;
use PaypalServerSdkLib\Models\CheckoutPaymentIntent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Url;

/**
 * Class of PaymentForm.
 */
class PaymentForm extends FormBase {

  /**
   * Drupal\basket_paypal\PayPal definition.
   *
   * @var \Drupal\basket_paypal\PayPal
   */
  protected $payPal;

  /**
   * Initializes the class container.
   */
  public function __construct() {
    $this->payPal = \Drupal::getContainer()->get('PayPal');
  }

  /**
   * Retrieves the form ID for the PayPal payment form.
   */
  public function getFormId() {
    return 'basket_paypal_payment_form';
  }

  /**
   * Builds and returns a form for processing payments.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = []) {
    if (empty($payment)) {
      throw new NotFoundHttpException();
    }
    $form['#id'] = 'payment_form';
    $form['#attached']['library'][] = 'basket_paypal/css';
    $form['info'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="sum"><label>{{ label }}:</label> {{ sum }} {{ currency }}</div>',
      '#context' => [
        'label' => $this->payPal->t('Sum'),
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->payPal->t('Pay'),
      ],
    ];

    $payment->result_url = Url::fromRoute('basket_paypal.pages', ['page_type' => 'result'], ['absolute' => TRUE])->toString();
    $payment->cancel_url = Url::fromRoute('basket_paypal.pages', ['page_type' => 'cancel'], ['absolute' => TRUE])->toString();

    // Payment alter:
    $this->basketPaymentFormAlter($form, $form_state, $payment);

    $form['info']['#context']['currency'] = $form['#params']['currency'];
    $form['info']['#context']['sum'] = $form['#params']['amount'];
    return $form;
  }

  /**
   * Handles the submission of a form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Alters the payment form for PayPal basket integration.
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {

    $config = $this->payPal->getConfig();
    $form['#params'] = [
      'amount' => $payment->amount,
      'currency' => $payment->currency,
      'order_id' => $payment->id . '-' . time() . rand(0, 999),
      'result_url' => $payment->result_url,
      'cancel_url' => $payment->cancel_url,
    ];

    // Alter:
    \Drupal::moduleHandler()->alter('basket_paypal_payment_params', $form['#params'], $payment, $config);

    $params = [
      'intent' => CheckoutPaymentIntent::CAPTURE,
      'purchase_units' => [
        PurchaseUnitRequestBuilder::init(
          AmountWithBreakdownBuilder::init(
            $payment->currency ?? 'USD',
            $payment->amount ?? 0,
          )->build(),
        )->referenceId($payment->id)->build(),
      ],
      'application_context' => OrderApplicationContextBuilder::init()
        ->cancelUrl($payment->cancel_url)
        ->returnUrl($payment->result_url)
        ->build(),
    ];

    // Alter:
    \Drupal::moduleHandler()->alter('basket_paypal_order_params', $params, $payment, $config);

    $_SESSION['basket_paypal_last_pay'] = $payment->id;
    $client = $this->payPal->client($config);
    $orderRequestBuilder = OrderRequestBuilder::init(
      $params['intent'],
      $params['purchase_units'],
    );

    if (!empty($params['application_context'])) {
      $orderRequestBuilder->applicationContext($params['application_context']);
    }

    if (!empty($params['payment_source'])) {
      $orderRequestBuilder->paymentSource($params['payment_source']);
    }

    $collect = [
      'body' => $orderRequestBuilder->build(),
      'prefer' => 'return=representation',
    ];
    try {
      // Call API with your client and get a response for your call.
      $response = $client->getOrdersController()->ordersCreate($collect);
      $result = $response->getResult();
      $links = $result->getLinks();

      if (!empty($links)) {
        foreach ($links as $link) {
          if ($link->getRel() === 'approve') {
            $form['#action'] = $link->getHref();
          }
        }
      }
      return $response;
    }
    catch (\Exception $ex) {
      \Drupal::logger('BasketPayPal')->notice($ex->getMessage());
    }
  }

}
