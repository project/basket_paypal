<?php

namespace Drupal\basket_paypal\Plugin\Basket\Payment;

/**
 * Class of BasketPaypalJsSDK.
 *
 * @BasketPayment(
 *   id = "basket_paypal_js",
 *   name = "Basket PayPal (JS SDK)",
 * )
 */
class BasketPaypalJsSDK extends BasketPaypal {

}
